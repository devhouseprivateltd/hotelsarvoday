import React from 'react';
import Navigation from './Component/Header/Navigation';
import { Switch, Redirect } from 'react-router-dom'

import Footer from './Component/Footer/Footer';


import routes from './Component/route/route' ;
import RouteWithSubRoutes from './shared/component/routeWithsubroute/RouteWithSubRoutes'
import routeList from './shared/constants/routes';

function App() {



  return (
    <>
      <Navigation />
      <Switch>
        <Redirect exact from="/" to={routeList.HOME.path} />
        {routes.map((route, i) => (
          <RouteWithSubRoutes key={i} {...route} />
        ))}
        
      </Switch>
      <Footer />
    </>
  );
}

export default App;

