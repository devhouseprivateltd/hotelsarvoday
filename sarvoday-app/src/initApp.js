import { Suspense } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ToastContainer } from "react-toastify";

import "flatpickr/dist/themes/material_green.css";
import "react-toastify/dist/ReactToastify.css";
import "./scss/main.css";

import App from "./App";
import Context from "./Component/store/Context";
import LazyLoader from "./Component/lazyLoader/LazyLoader";

const initApp = () => {
  return (
    <Context>
      <Suspense fallback={<LazyLoader />}>
        <ToastContainer
          position="bottom-center"
          autoClose={3000}
          hideProgressBar
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <Router>
          <Switch>
            <Route path="/" component={App} />
          </Switch>
        </Router>
      </Suspense>
    </Context>
  );
};
export default initApp;
