export const setLoginData = (userObj) => {
  localStorage.setItem("sarvoday_token", userObj.token);
  localStorage.setItem("sarvoday_user", JSON.stringify(userObj.user));
};

export const getAccessToken = () => {
  return localStorage.getItem("sarvoday_token");
};

export const getUser = () => {
  return JSON.parse(localStorage.getItem("sarvoday_user"));
};

export const setUser = (user) => {
  localStorage.setItem("sarvoday_user", JSON.stringify(user));
};

export const clearLoginData = () => {
  localStorage.removeItem("sarvoday_token");
  localStorage.removeItem("sarvoday_user");
};
