import React, { useRef, useState, useContext } from "react";
import { useForm } from "react-hook-form";
import ReservationFromRoomCard from "./ReservationFromRoomCard";
import { MainContest } from "../../store/Context";
import ReservationFormShowRoomName from "./ReservationFormShowRoomName";
import { message } from "antd";
import { useHistory } from "react-router-dom";

export default function ReservationForm() {
  const Ctx = useContext(MainContest);
  const history = useHistory();
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();
  const {
    register: register2,
    handleSubmit: handleSubmit2,
    formState: { errors: errors2 },
  } = useForm();

  const email = useRef({});
  // const companyEmail = useRef({});
  email.current = watch("email", "");
  const [newLabel, setLabel] = useState(false);
  const [bookFor, setBookFor] = useState(false);
  const [terms, setTerms] = useState(false);
  console.log(Ctx.roomDetails, "rooms");

  const onSubmit = (data) => {
    console.log(data);
    message.success(`you are sucessflly booked `);
    history.push("/");
  };

  return (
    <div class="container">
      <div class="roomDetails">
        <h2>Your Selection</h2>
        <div class="bookDetails">
          <h5>Your booking details</h5>
          <div class="dates">
            <div class="check">
              <h6>check-in-out Date </h6>
              <h5>{Ctx.homePageValue && Ctx.homePageValue.date}</h5>
              {/* <p>From 12:00 AM</p> */}
            </div>
          </div>
          <div class="totalDays">
            <h6>Total length of stay:</h6>
            <h5>31 nights</h5>
            <a href="##">Travelling on different dates?</a>
          </div>
          <div class="selectedRooms">
            <h4>Your selection</h4>
            <div class="roomsCard">
              {Ctx.roomDetails.map((item) => (
                <ReservationFormShowRoomName key={item.id} data={item} />
              ))}
            </div>
            <a href="##"> Change Selection?</a>
          </div>
          <div class="pricing">
            <div class="heading highlight">
              <h4>
                <span>Price</span> <span>₹ 35,000</span>
              </h4>
              <p>(For 31 nights & all guests)</p>
            </div>
            <div class="heading">
              <h4>Additional Charges</h4>
              <p>
                <span>Goods & services tax</span> <span>₹ 1,000</span>
              </p>
            </div>
          </div>
          <div class="pricing">
            <div class="heading">
              <h4>Your payment schedule</h4>
              <p>
                To avoid cancellation you'll pay <span> ₹ 26,250</span>{" "}
              </p>
            </div>
            <div class="heading">
              <h4>Coupons & Discounts</h4>
              <p>
                <span>Coupons Applied</span> <span>- ₹ 2,000</span>
              </p>
              <p>
                <span>Discount</span> <span>- ₹ 8,000</span>
              </p>
            </div>
          </div>
          {/* <!-- <div class="pricing">
                  <div class="heading">
                    <h4>How much to cancel?</h4>
                    <p>Free cancellationu until <span>23:59 on 1 Jan 2022</span></p>
                    <p>From 00:00 on 2 Jan 2022 <span>₹ 1,000</span></p>
                  </div>
                  
                </div> --> */}
        </div>
      </div>

      <div class="bookings">
        <div class="boxes">
          <h4>Good to know:</h4>
          <p>
            <ion-icon name="card-outline"></ion-icon>
            No credit card needed to book a room!
          </p>
        </div>
        <div class="boxes">
          <h5>
            <ion-icon name="person-outline"></ion-icon>
            <a href="##">Sign in</a> &nbsp;to book with your previous details
            or&nbsp;<a href="##">register</a> &nbsp;to book on the go.
          </h5>
        </div>

        <div class="boxes">
          <h4>Enter your details</h4>
          <p class="badge">
            Almost done! just fill in the <span>&nbsp;*&nbsp;</span> required
            fields
          </p>
          <form
            id="bookForm"
            onSubmit={
              newLabel ? handleSubmit2(onSubmit) : handleSubmit(onSubmit)
            }
            class="bookingForm"
          >
            <div class="inputFields">
              <p>Are you travelling for work?</p>
              <div class="split">
                <div class="radio">
                  <input
                    type="radio"
                    name="work"
                    id="workYes"
                    value="workYes"
                    onChange={() => setLabel(true)}
                  />
                  <label for="workYes">Yes</label>
                </div>
                <div class="radio">
                  <input
                    type="radio"
                    name="work"
                    id="WorkNo"
                    value="workNo"
                    onChange={() => setLabel(false)}
                  />
                  <label for="workNo">No</label>
                </div>
              </div>
            </div>
            <div class="inputFields">
              <div class="split">
                <div class="field">
                  <label for="fName">
                    {newLabel ? "Company Name" : "First Name"}
                    <span>*</span>
                  </label>

                  {newLabel ? (
                    <input
                      type="text"
                      {...register2("companyName", {
                        required: "This field is important",
                      })}
                    />
                  ) : (
                    <input
                      type="text"
                      {...register("fName", {
                        required: "This field is important",
                      })}
                    />
                  )}

                  {errors2.companyName && (
                    <span className="inputError">
                      {errors2.companyName.message}
                    </span>
                  )}
                  {errors.fName && (
                    <span className="inputError">{errors.fName.message}</span>
                  )}
                </div>
                <div class="field">
                  <label for="lName">
                    {newLabel ? "Your Name" : "Last Name"} <span>*</span>
                  </label>
                  {newLabel ? (
                    <input
                      type="text"
                      {...register2("employeeName", {
                        required: "This field is important",
                      })}
                    />
                  ) : (
                    <input
                      type="text"
                      {...register("lName", {
                        required: "This field is important",
                      })}
                    />
                  )}

                  {errors2.employeeName && (
                    <span className="inputError">
                      {errors2.employeeName.message}
                    </span>
                  )}
                  {errors.lName && (
                    <span className="inputError">{errors.lName.message}</span>
                  )}
                </div>
              </div>
            </div>
            <div class="inputFields">
              <div class="split">
                <div class="singleField">
                  <label for="email">
                    {newLabel ? "Company Email" : "Email"} <span>*</span>
                  </label>
                  {newLabel ? (
                    <input
                      {...register2("companyEmail", {
                        required: "This field is required",
                        pattern: {
                          value:
                            /^[a-zA-Z0-9.!#$%&’+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)$/,
                          message: "Please Enter a valid email address.",
                        },
                      })}
                    />
                  ) : (
                    <input
                      type="text"
                      {...register("email", {
                        required: "This field is important",
                        pattern: {
                          value:
                            /^[a-zA-Z0-9.!#$%&’+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)$/,
                          message: "Please Enter a valid email address.",
                        },
                      })}
                    />
                  )}

                  {errors2.companyEmail && (
                    <span className="inputError">
                      {errors2.companyEmail.message}
                    </span>
                  )}

                  {errors.email && (
                    <span className="inputError">{errors.email.message}</span>
                  )}
                  <p>Confirmation email goes to this address</p>
                </div>
              </div>
            </div>
            <div class="inputFields">
              <div class="split">
                <div class="singleField">
                  <label for="cEmail">
                    Confirm Email <span>*</span>
                  </label>
                  <input
                    type="email"
                    name="cEmail"
                    {...register("cEmail", {
                      required: "This field is required",

                      validate: (value) =>
                        value === email.current || "email doesn't match",
                    })}
                  />

                  {errors.cEmail && (
                    <span className="inputError">{errors.cEmail.message}</span>
                  )}
                </div>
              </div>
            </div>
            <div class="inputFields">
              <p>Who are you booking for?</p>
              <div class="split splitCol">
                <div class="radio">
                  <input
                    type="radio"
                    name="book-for"
                    id=""
                    onChange={() => setBookFor(false)}
                  />
                  <p>For myself</p>
                </div>
                <div class="radio">
                  <input
                    type="radio"
                    name="book-for"
                    id=""
                    onChange={() => setBookFor(true)}
                  />
                  <p>For someone else</p>
                </div>
              </div>
            </div>
          </form>
        </div>

        <div class="boxes">
          {Ctx.roomDetails.map((item) => (
            <ReservationFromRoomCard
              bookFor={bookFor}
              data={item}
              key={item.id}
            />
          ))}
        </div>

        <div class="boxes">
          <div class="termsNbtn">
            <div class="terms">
              <input
                type="checkbox"
                name=""
                id="terms"
                onChange={() => setTerms(!terms)}
              />
              <label for="terms">
                By clicking this you'll agree our{" "}
                <a href="##">terms and conditions</a>{" "}
              </label>
            </div>
            <div class="confirmBtn">
              {terms && (
                <button form="bookForm" type="submit" class="confirm">
                  Final: Make Payment
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
