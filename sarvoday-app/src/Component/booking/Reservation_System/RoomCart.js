import React, { useContext } from "react";
import { MainContest } from "../../store/Context";

function RoomCart({ data }) {
  const { id, catagoryName, description, price, avalibleRooms, RoomImage,roomBed,maxPeople} =
    data;
  const Ctx = useContext(MainContest);
  const onAddRoomDetails = () => {
        Ctx.setRoomDetails((prevRoomDetails,ind) => {
          // console.log(prevRoomDetails);
          return [
            ...prevRoomDetails,
            {
              id: id,
              catagoryName: catagoryName,
              description: description,
              price: price,
              avalibleRooms: avalibleRooms,
              RoomImage: RoomImage,
              roomBed:roomBed,
              maxPeople:maxPeople,
            },
          ];
        });
  };
  return (
    <div className="rooms">
      <div
        className="roomImg"
        style={{
          backgroundImage: `url(
            "https://www.eliaermouhotel.com/uploads/photos/D1024/2019/02/standardroom_1878.jpg"
          )`
        }}
      ></div>
      <div className="details">
        <div className="header">
          <div className="title">
            <h4>{catagoryName}</h4>
          </div>
          <div className="rating">
            <p>
              Rating <br />
              <span>188 Peoples</span>
            </p>
            <p className="badge">8.7</p>
          </div>
        </div>
        <div className="description">
          <div className="roomDesc">
            <p>
              {description}
            </p>
            <ul>
              <li>
                <ion-icon name="bed"></ion-icon>2 Beds
              </li>
              <li>
                <ion-icon name="person"></ion-icon>3 Peoples
              </li>
              <li>
                <ion-icon name="image"></ion-icon>1 Balcony
              </li>
              <li>
                <ion-icon name="thermometer" className=""></ion-icon>
                <ion-icon name="wifi" className="not"></ion-icon>
                <ion-icon name="tv"></ion-icon>
              </li>
            </ul>
          </div>
          <div className="priceDesc">
            <div className="dayPeople">
              <p>31 Nights 2 Peoples</p>
            </div>
            <div className="price">
              <p className="badge">Limited Time Offer</p>
              <h6>{(price * 1.2)}</h6>
              <h4>{price}</h4>
              <p>Including all taxs</p>
            </div>
            <div className="book-btn">
              <button onClick={onAddRoomDetails} >Add This Room</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default RoomCart;
