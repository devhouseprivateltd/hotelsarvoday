
const ReservationFormShowRoomName =({data})=>{
    const {catagoryName,noOfRooms} = data
    return <h5>{noOfRooms} x {catagoryName}</h5>;
}
export default ReservationFormShowRoomName;