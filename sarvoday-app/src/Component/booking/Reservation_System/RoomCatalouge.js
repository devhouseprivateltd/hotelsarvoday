import React, { useState, useContext, useEffect } from "react";
import RoomCart from "./RoomCart";
import RoomTable from "./RoomTable";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import SearchFilter from "./SearchFilter";
import { MainContest } from "../../store/Context";
import routes from "../../../shared/constants/routes";

const DUMMY_DATA = [
  {
    id: 1,
    catagoryName: "Dulax",
    description:
      "2 Adults, 1 king size bed, 1 large screen tv, 1 bathroom, Balcony with superior view ",
    roomBed: 3,
    maxPeople: 3,
    price: "3999",
    avalibleRooms: 7,
    RoomImage: "image link",
  },
  {
    id: 2,
    catagoryName: "Super Dulax",
    description:
      "2 Adults, 1 child, 1 king size bed, 1 small bed, 1 large screen tv, 1 large bathroom, Balcony with superior view ",
    roomBed: 2,
    maxPeople: 2,
    price: "4999",
    avalibleRooms: 12,
    RoomImage: "image link",
  },
  {
    id: 3,
    catagoryName: "Honeymoon suite",
    description:
      "2 Adults, 1 king size bed, 1 large screen tv, 1 bathroom, Balcony with superior view, fully customizable for newly weds cupels ",
    roomBed: 3,
    maxPeople: 3,
    price: "4499",
    avalibleRooms: 5,
    RoomImage: "image link",
  },
  {
    id: 4,
    catagoryName: "Executive",
    description:
      "2 Adults, 1 king size bed, 1 large screen tv, 1 bathroom, Balcony with superior view ",
    roomBed: 2,
    maxPeople: 2,
    price: "3499",
    avalibleRooms: 9,
    RoomImage: "image link",
  },
  {
    id: 5,
    catagoryName: "Super Executive",
    description:
      "2 Adults, 1 child, 1 king size bed, 1 small bed, 1 large screen tv, 1 large bathroom, Balcony with superior view ",
    roomBed: 3,
    maxPeople: 3,
    price: "4499",
    avalibleRooms: 2,
    RoomImage: "image link",
  },
  {
    id: 6,
    catagoryName: "Super Executive 3",
    description:
      "2 Adults, 1 child, 1 king size bed, 1 small bed, 1 large screen tv, 1 large bathroom, Balcony with superior view ",
    roomBed: 3,
    maxPeople: 3,
    price: "4999",
    avalibleRooms: 15,
    RoomImage: "image link",
  },
];

export default function RoomCatalouge() {
  const Ctx = useContext(MainContest);
  const history = useHistory();
  const [totalPrice, setTotalPrice] = useState(0);

  useEffect(() => {
    const totalPriceCal = () => {
      let sum = 0;
      Ctx.roomDetails.map((value) => (sum += parseInt(value.price)));
      setTotalPrice(sum);
    };
    totalPriceCal();
  }, [Ctx.roomDetails, setTotalPrice]);

  const onSubmit = () => {
    history.push(routes.RESERVSTION_FORM.path);
  };

  return (
    <>
      <div className="container">
        <SearchFilter />
        <div className="roomsCards">
          <h3>
            Avalable Rooms: <span>26 Rooms</span>
          </h3>
          {DUMMY_DATA.map((item) => (
            <RoomCart data={item} key={item.id} />
          ))}
        </div>
      </div>
      <div className="detailsTable">
        <table className="selectedRooms">
          <thead>
            <tr>
              <th>#</th>
              <th colSpan="2">Room type</th>
              <th>No of rooms</th>
              <th>Pricing</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <RoomTable />
            <tr></tr>
            <tr></tr>
            <tr>
              <td colSpan={4}>
                <p className="message">
                  To view selected details and verify it click on confirm
                  details{" "}
                </p>
              </td>
              <td>
                <div className="totalPrice">
                  <h4>
                    Total Price: <span>₹ {totalPrice}</span>
                  </h4>
                </div>
              </td>
              <td>
                <button onClick={onSubmit} className="bookBtn">
                  Confirm Details
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </>
  );
}
