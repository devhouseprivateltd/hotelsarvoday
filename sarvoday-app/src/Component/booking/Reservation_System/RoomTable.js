import React, { useContext } from "react";
import { MainContest } from "../../store/Context";
import Select from "react-select";

function RoomTable() {
  const { roomDetails, setRoomDetails } = useContext(MainContest);

  let newRoomDetails = roomDetails;
  const addDeleteHandler = (id) => {
    console.log("del");
    const newRoomDetails = roomDetails.filter((ele) => {
      return id !== ele.id;
    });
    setRoomDetails(newRoomDetails);
  };
  const rooms = [
    { value: "1", label: "1 Rooms" },
    { value: "2", label: "2 Rooms" },
    { value: "3", label: "3 Rooms" },
  ];

  console.log(newRoomDetails);

  const setRoomInfo = (data, id) => {
    newRoomDetails.map((item) => {
      if (item.id === id) {
         item.noOfRooms = data.value;
      }
      return item;
    });
    // console.log(newRoomDetails,'new');
  };

  return (
    <>
      {roomDetails.map(
        (
          { id, catagoryName, description, price, avalibleRooms, RoomImage },
          ind
        ) => {
          return (
            <tr key={id}>
              <td>{ind + 1}</td>
              <td colSpan="2">
                <h4>{catagoryName}</h4>
                <p>
                  Available Rooms: <span>{avalibleRooms}</span>
                </p>
              </td>
              <td>
                <Select
                  options={rooms}
                  onChange={(data) => setRoomInfo(data, id)}
                />
              </td>

              <td>
                <span>₹{price * 1.2}</span> ₹ {price}
              </td>
              <td>
                <button
                  className="closeBtn"
                  onClick={() => addDeleteHandler(id)}
                >
                  <ion-icon name="close"></ion-icon>
                </button>
              </td>
            </tr>
          );
        }
      )}
    </>
  );
}

export default RoomTable;
