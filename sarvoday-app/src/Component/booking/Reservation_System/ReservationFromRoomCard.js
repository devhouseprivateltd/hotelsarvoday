import React from "react";
import { useForm } from "react-hook-form";
export default function ReservationFromRoomCard({bookFor, data}) {

    const {
        register,
        // handleSubmit,
        // watch,
        formState: { errors },
      } = useForm();

    console.log(data.roomBed);

    const runbeds= (runBeds) =>{
        return runBeds();
    }
    const runpersons= (cp) =>{
        return cp();
    }
    
      

    return(
        <>
            <div class="roomPreview">
            <h4>
              {data.catagoryName}<span>x2</span>
            </h4>

            <h5>
              Available beds: &nbsp;

                 {

                    runbeds(() => {
                        const beds= [];
                        for( let i = 0; i < data.roomBed; i++){

                          if(data.roomBed === 3 ){
                            beds.push (<p key={i}></p>);
                          } 

                         
                          beds.push (<ion-icon key={i} name="bed-outline" ></ion-icon>);
                        
                     }
                     return beds
                    })


                    
                }

            </h5>
            <h5>
              Max Persons: &nbsp;
              {
                runpersons(() => {
                        const person= [];
                        for( let j = 0; j < data.maxPeople; j++){
                          if(data.maxPeople === 3)
                          {
                            person.push (<p key={j}></p>);
                          }
                        person.push (<ion-icon key={j} name="person-outline" ></ion-icon>);
                     }
                     return person
                    })
              }
            </h5>
            <form class="guestInfo">
              <div class="inputFields">
                <label for="gName">Full guest name</label>
                <input type="text" id="gName" />
              </div>
              {bookFor ? (
                <div class="inputFields">
                  <label for="gEmail">
                    Guest email <span>*</span>
                  </label>
                  <input
                    type="email"
                    {...register("gEmail", {
                      required: "This field is required",
                      pattern: {
                        value:
                          /^[a-zA-Z0-9.!#$%&’+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)$/,
                        message: "Please Enter a valid email address.",
                      },
                    })}
                  />
                  {errors.gEmail && <span className="inputError">{errors.gEmail.message}</span>}
                </div>
              ) : (
                ""
              )}
            </form>
          </div>
        </>
    )
};
