import React from "react";
import Flatpickr from "react-flatpickr";
import Select from "react-select";
import { useContext } from "react";
import { MainContest } from "../../store/Context";
function SearchFilter() {
  const Ctx = useContext(MainContest);
  console.log(Ctx.homePageValue.date);
  const rooms = [
    { value: "1", label: "1 Rooms" },
    { value: "2", label: "2 Rooms" },
    { value: "3", label: "3 Rooms" },
  ];

  return (
    <div className="filter">
      <h4>Search</h4>
      <div className="inputBox">
        <div className="inputField">
          <label for="checkIn">Check-In-Out</label>
          <Flatpickr
            options={{
              defaultDate: Ctx.homePageValue.date,
              mode: "range",
              minDate: "today",
              dateFormat: "Y-m-d",
              value: "today",
              altFormat: "F j, Y",
              // onChange: (r,date) => setDate(date),
            }}
          />
        </div>
        <div className="inputField">
          <div>
            <div className="adult">
              <label for="adult">Adult</label>
              <Select
                name="adult"
                id="adult"
                defaultValue={{
                  label: `${Ctx.homePageValue.child} adult`,
                  value: Ctx.homePageValue.adult,
                }}
                options={rooms}
              ></Select>
            </div>
            <div className="child">
              <label for="child">Child</label>
              <Select
                name="child"
                id="child"
                defaultValue={{
                  label: `${Ctx.homePageValue.child} child`,
                  value: Ctx.homePageValue.room,
                }}
                options={rooms}
              ></Select>
            </div>
          </div>
        </div>
        <div className="inputField">
          <div>
            <div className="rooms">
              <label for="rooms">Rooms</label>
              <Select
                name="rooms"
                id="rooms"
                defaultValue={{
                  label: `${Ctx.homePageValue.noOfRooms} Rooms`,
                  value: Ctx.homePageValue.room,
                }}
                options={rooms}
              ></Select>
            </div>
          </div>
        </div>
        <div className="inputField">
          <button className="Search">Search</button>
        </div>
      </div>
    </div>
  );
}

export default SearchFilter;
