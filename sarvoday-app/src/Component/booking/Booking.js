import React, { useContext, useState } from "react";
import Flatpickr from "react-flatpickr";
import { useForm } from "react-hook-form";
import { MainContest } from "../store/Context";
import { useHistory } from "react-router-dom";
import { routeConstants } from "../../shared/constants";

const Booking = (props) => {
  const Ctx = useContext(MainContest);
  const [date, setDate] = useState();
  const { register, getValues } = useForm();
  const history = useHistory();
  // const [departure, setDeparture] = useState(false);

  const onGetAllValues = (e) => {
    e.preventDefault();
    const allValues = {
      date,
      adults: getValues("adults"),
      child: getValues("child"),
      noOfRooms: getValues("noOfRooms"),
    };
    Ctx.setHomepageValue(allValues);
    history.push("/room-catalouge");
  };

  return (
    <>
      <div className="bookingContainer">
        <div className="bookingHeader">
          <div className="bookingButton activeBtn">
            <p>Book Your Stay</p>
          </div>
          <div className="bookingButton">
            <a href={routeConstants.RESTURENT.path}>
              <p>Book Your Table</p>
            </a>
          </div>
        </div>
        <form className="bookingBody">
          <div className="inputField">
            <ion-icon name="calendar-outline"></ion-icon>
            <Flatpickr
              options={{
                mode: "range",
                minDate: "today",
                dateFormat: "Y-m-d",
                value: "today",
                altFormat: "F j, Y",
                onChange: (r, date) => setDate(date),
              }}
              placeholder="Check In - Check Out"
            />
          </div>

          <div className="inputField">
            <ion-icon name="person-outline"></ion-icon>
            <input
              type="number"
              placeholder="Adults"
              {...register("adults")}
            ></input>
            <input
              type="number"
              placeholder="child's"
              {...register("child")}
            ></input>
            <input
              type="number"
              placeholder="Rooms"
              {...register("noOfRooms")}
            ></input>
          </div>

          <div className="inputField">
            <button className="submit" onClick={onGetAllValues}>
              Check Availability
            </button>
          </div>
        </form>
      </div>
    </>
  );
};
export default Booking;
