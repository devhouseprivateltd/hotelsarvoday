import React from "react";
import "./Resturent.css";
import noodles from "../../../src/assets/img/ResturentImg/noodle.png";
import catagories1 from  '../../../src/assets/img/ResturentImg/catagories-1.jpg';
import catagories2 from "../../../src/assets/img/ResturentImg/catagories-2.jpg";
import catagories3 from "../../../src/assets/img/ResturentImg/catagories-3.jpg";
import featured1 from "../../../src/assets/img/ResturentImg/featured-1.jpg";
import featured2 from "../../../src/assets/img/ResturentImg/featured-2.jpg";
import featured3 from "../../../src/assets/img/ResturentImg/featured-3.jpg";
import featured4 from "../../../src/assets/img/ResturentImg/featured-4.jpg";
import recent1 from "../../../src/assets/img/ResturentImg/recent-1.jpg";
import recent2 from "../../../src/assets/img/ResturentImg/recent-2.jpg";
import recent3 from "../../../src/assets/img/ResturentImg/recent-3.jpg";
import recent4 from "../../../src/assets/img/ResturentImg/recent-4.jpg";
import recent5 from "../../../src/assets/img/ResturentImg/recent-5.jpg";
import recent6 from "../../../src/assets/img/ResturentImg/recent-6.jpg";
import recent7 from "../../../src/assets/img/ResturentImg/recent-7.jpg";
import recent8 from "../../../src/assets/img/ResturentImg/recent-8.jpg";
import offer from "../../../src/assets/img/ResturentImg/offer.png";
import userprofile1 from "../../../src/assets/img/ResturentImg/userprofile-1.jpg";
import userprofile2 from "../../../src/assets/img/ResturentImg/userprofile-2.jpg";
import userprofile3 from "../../../src/assets/img/ResturentImg/userprofile-3.jpg";
import brands1 from "../../../src/assets/img/ResturentImg/brands-1.svg";
import brands2 from "../../../src/assets/img/ResturentImg/brands-2.svg";
import brands3 from "../../../src/assets/img/ResturentImg/brands-3.svg";
import brands4 from "../../../src/assets/img/ResturentImg/brands-4.svg";
import brands5 from "../../../src/assets/img/ResturentImg/brands-5.svg";







export default function ResturentHome() {
  return (
    <>
      <div className="resBanner">
        <div className="row">
          <div className="col-2">
            <h1>
              Eat , Sleep
              <br />
              &amp; Repeat
            </h1>
            <p>
              Eating is not merely a material pleasure. Eating well gives a
              spectacular joy to life and contributes immensely to goodwill and
              happy companionship. It is of great importance to the morale
            </p>
            <bttn>
              <a href className="btn">
                Start Eating →
              </a>
            </bttn>
          </div>
          <div className="col-2">
            <img src={noodles} alt="" />
          </div>
        </div>
      </div>

      {/* <!-- -------- featured catagories -------- --> */}
      <div className="catagories">
        <h1>Special Items</h1>
        <div className="small-container">
          <div className="row">
            <div className="col-3">
              <img src={catagories1} alt="" />
            </div>
            <div className="col-3">
              <img src={catagories2} alt="" />
            </div>
            <div className="col-3">
              <img src={catagories3} alt="" />
            </div>
          </div>
        </div>
      </div>

      {/* <!-- -------featured products-------- --> */}
      <div className="small-container">
        <h2 className="title">Featured Items</h2>
        <div className="row">
          <div className="col-4">
            <img src={featured1} alt="" />
            <h4>Steamed Biryani</h4>
            <div className="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half"></i>
            </div>
            <p>Rs. 250.00</p>
            <a href className="cart-btn">
              Add to Cart
            </a>
          </div>
          <div className="col-4">
            <img src={featured2} alt="" />
            <h4>Chicken Curry</h4>
            <div className="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i className="fa fa-star-o" />
            </div>
            <p>Rs. 180.00</p>
            <a href className="cart-btn">
              Add to Cart
            </a>
          </div>
          <div className="col-4">
            <img src={featured3} alt="" />
            <h4>Masala Dosa</h4>
            <div className="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half"></i>
              <i className="fa fa-star-o" />
            </div>
            <p>Rs. 150.00</p>
            <a href className="cart-btn">
              Add to Cart
            </a>
          </div>
          <div className="col-4">
            <img src={featured4} alt="" />
            <h4>Chicken Sandwich</h4>
            <div className="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i className="fa fa-star-o" />
            </div>
            <p>Rs. 80.00</p>
            <a href className="cart-btn">
              Add to Cart
            </a>
          </div>
        </div>
        <h2 className="title">Recent Addition</h2>
        <div className="row">
          <div className="col-4">
            <img src={recent1} alt="" />
            <h4>Vegetable Chop</h4>
            <div className="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i className="fa fa-star-o" />
            </div>
            <p>Rs. 20.00</p>
            <a href className="cart-btn">
              Add to Cart
            </a>
          </div>
          <div className="col-4">
            <img src={recent2} alt="" />
            <h4>Chicken Finger</h4>
            <div className="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half"></i>
            </div>
            <p>Rs. 50.00</p>
            <a href className="cart-btn">
              Add to Cart
            </a>
          </div>
          <div className="col-4">
            <img src={recent3} alt="" />
            <h4>Chicken Chap</h4>
            <div className="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half"></i>
              <i className="fa fa-star-o" />
            </div>
            <p>Rs. 100.00</p>
            <a href className="cart-btn">
              Add to Cart
            </a>
          </div>
          <div className="col-4">
            <img src={recent4} alt="" />
            <h4>Chicken Matar</h4>
            <div className="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i className="fa fa-star-o" />
              <i className="fa fa-star-o" />
            </div>
            <p>Rs. 180.00</p>
            <a href className="cart-btn">
              Add to Cart
            </a>
          </div>
        </div>
        <div className="row">
          <div className="col-4">
            <img src={recent5} alt="" />
            <h4>Tarka Roti</h4>
            <div className="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half"></i>
            </div>
            <p>Rs. 120.00</p>
            <a href className="cart-btn">
              Add to Cart
            </a>
          </div>
          <div className="col-4">
            <img src={recent6} alt="" />
            <h4>Fried Rice</h4>
            <div className="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half"></i>
              <i className="fa fa-star-o" />
            </div>
            <p>Rs. 180.00</p>
            <a href className="cart-btn">
              Add to Cart
            </a>
          </div>
          <div className="col-4">
            <img src={recent7} alt="" />
            <h4>Egg Curry</h4>
            <div className="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half"></i>
            </div>
            <p>Rs. 150.00</p>
            <a href className="cart-btn">
              Add to Cart
            </a>
          </div>
          <div className="col-4">
            <img src={recent8} alt="" />
            <h4>Mutton Curry</h4>
            <div className="rating">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i className="fa fa-star-o" />
            </div>
            <p>Rs. 200.00</p>
            <a href className="cart-btn">
              Add to Cart
            </a>
          </div>
        </div>
      </div>

      <div>
        {/* -----offer--- */}
        <div className="offer">
          <div className="small-container">
            <div className="row">
              <div className="col-2">
                <img src={offer} alt="" className="offer-img" />
              </div>
              <div className="col-2">
                <p>Exclusively Avalable on Food Point</p>
                <h1>Mutton Biryani</h1>
                <div className="small">
                  Tere liye me is jahan se lad jaunga
                  <br /> Tu bol to sahi tujhe duniya ki sabse achi Biryani khila
                  launga..
                  <br />
                  Best test health ki tension you can take rest
                </div>
                <a href className="btn">
                  Get it →
                </a>
              </div>
            </div>
          </div>
        </div>
        {/* -------testimonial-------- */}
        <div className="testimonial">
          <div className="small-container">
            <div className="row">
              <div className="col-3">
                <i className="fa fa-quote-left" />
                <p>
                  If you want to eat best the only way is Food Point. Best
                  foods, great service, affordable price and Zero tension.
                </p>
                <div className="rating">
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star-half"></i>
                </div>
                <img src={userprofile1} alt="" />
                <h3>Amiya Das</h3>
              </div>
              <div className="col-3">
                <i className="fa fa-quote-left" />
                <p>
                  Love to eat good foods. I've tried most of the avalable
                  services but this one! ooh man this one really great. I highly
                  recommend just give it a try.
                </p>
                <div className="rating">
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star-half"></i>
                </div>
                <img src={userprofile2} alt="" />
                <h3>Kartick Sur</h3>
              </div>
              <div className="col-3">
                <i className="fa fa-quote-left" />
                <p>
                  Food is the only thing that everyone wanted to be the best. I
                  tried food point. I really love there food quality and
                  quantity.
                </p>
                <div className="rating">
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star-half"></i>
                </div>
                <img src={userprofile3} alt="" />
                <h3>Sanket Mukherjee</h3>
              </div>
            </div>
          </div>
        </div>
        {/* -------brands------- */}
        <div className="brands">
          <div className="small-container">
            <div className="row">
              <div className="col-5">
                <img src={brands1} alt="" />
              </div>
              <div className="col-5">
                <img src={brands2} alt="" />
              </div>
              <div className="col-5">
                <img src={brands3} alt="" />
              </div>
              <div className="col-5">
                <img src={brands4}  alt="" />
              </div>
              <div className="col-5">
                <img src={brands5}  alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
