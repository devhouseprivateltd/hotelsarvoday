
import HomeRoute from "./HomeRoute";
import GalleryRoute from "./GalleryRoute";
import ReservationFormRoute from './ReservationForm';
import RoomCatalogueRoute from './RoomCatalougeRoute';
import ResturentRoute from "./ResturentRoute";
import LoginRoute from "./LoginRoute"
import RegistrationRoute from "./RegistrationRoute";

const screenRoutes = [
  HomeRoute,
  RoomCatalogueRoute,
  ReservationFormRoute,
  GalleryRoute,
  ResturentRoute,
  LoginRoute,
  RegistrationRoute,
];

export default screenRoutes;
