import React from 'react';
import { routeConstants } from "../../shared/constants";
const ReservationFormComponent = React.lazy(()=>import ('../UI/ReservationForm'))

const ReservationForm ={
    ...routeConstants.RESERVSTION_FORM,
    component:ReservationFormComponent,
}
export default ReservationForm;