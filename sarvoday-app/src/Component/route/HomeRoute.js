import React from 'react';
import { routeConstants } from "../../shared/constants";
const HomeComponent = React.lazy(()=>import('../UI/Home'))

const HomeRoute={
    ...routeConstants.HOME,
    component:HomeComponent
}
export default HomeRoute;