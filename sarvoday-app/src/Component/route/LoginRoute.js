import React from 'react';
import { routeConstants } from "../../shared/constants";
const Login = React.lazy(() => import("../Auth/Login/Login"));

const LoginRoute = {
  ...routeConstants.AUTH.subRoutes.LOGIN.path,
  component: Login,
};
export default LoginRoute;