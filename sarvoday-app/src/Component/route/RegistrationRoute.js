
import React from "react";
import { routeConstants } from "../../shared/constants";
const registration = React.lazy(() => import("../Auth/Login/Login"));

const RegistrationRoute = {
  ...routeConstants.AUTH.subRoutes.REGISTRATION.path,
  component: registration,
};
export default RegistrationRoute;