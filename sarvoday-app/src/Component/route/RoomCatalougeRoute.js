import React from 'react';
import { routeConstants } from "../../shared/constants";
const RoomCatalougeComponet = React.lazy(()=>import ('../UI/RoomCatalouge'))

const RoomCatalouge ={
    ...routeConstants.ROOM_CATALOGUE,
    component:RoomCatalougeComponet,
}
export default RoomCatalouge;