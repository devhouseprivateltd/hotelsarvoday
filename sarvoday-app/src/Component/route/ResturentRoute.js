import React from 'react';
import { routeConstants } from "../../shared/constants";
const ResturentComponent = React.lazy(()=>import ('../Resturent/ResturentHome'))


const ResturentRoute= {
    ...routeConstants.RESTURENT,
    component:ResturentComponent,
}
export default ResturentRoute;