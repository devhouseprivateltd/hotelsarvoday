import React from 'react';
import { routeConstants } from "../../shared/constants";
const GalleryComponent = React.lazy(()=>import ('../UI/Gallery'))


const GalleryRoute= {
    ...routeConstants.GALLARY,
    component:GalleryComponent,
}
export default GalleryRoute;