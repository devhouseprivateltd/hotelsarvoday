import React from "react";
import "./Login.css";

export default function Login() {
  return (
    <>
      <div className="loginContainer">
        <div className="loginBody">
          <div className="loginHeader">
            <h3>Log In</h3>
          </div>
          <form className="loginContent">
            <div className="inputField">
              <label for="email">
                <ion-icon name="mail-outline"></ion-icon>
              </label>
              <input
                type="email"
                id="email"
                placeholder="Enter your email"
              ></input>
            </div>
            <div className="inputField">
              <label for="password">
                <ion-icon name="lock-open-outline"></ion-icon>
              </label>
              <input
                type="password"
                id="password"
                placeholder="Enter password"
              ></input>
            </div>
            <div className=" checkField">
              <input type="checkbox"></input>
              <label>Remember me</label>
            </div>
            <div className="inputField btnField">
              <button type="submit">Login</button>
            </div>
          </form>
          <div className="divider">
            <span>&nbsp;</span>
            <p>Or</p>
            <span>&nbsp;</span>
          </div>
          <div className="otherContent">
            <div className="inputField">
              <button>
                <ion-icon name="logo-google"></ion-icon>
              </button>
              <button>
                <ion-icon name="logo-facebook"></ion-icon>
              </button>
            </div>
            <div className="inputField">
              <a href="/register">Forgot password?</a>
            </div>
            <div className="inputField">
              <a href="/register">New here? Register</a>
            </div>
          </div>
        </div>

        <div className="textBody">
          <h4>Welcome To</h4>
          <h1>Sarvoday</h1>
        </div>
      </div>
    </>
  );
}
