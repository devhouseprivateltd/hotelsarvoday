import React from 'react';
import Booking from '../booking/Booking';
import AccomodationSlider from '../carousel/AccomodationCarousel/AccomodationSlider';
import Carousel from '../carousel/Carousel';
import TextCarousel from '../carousel/TextCarousel';

export default function Home() {
    return(
        <>
        <div className="homeBody">
            <Carousel />
            <Booking />
            <TextCarousel />
            <AccomodationSlider />
            {/* <RoomCatalouge /> */}
            {/* <ReservationForm /> */}
            {/* <ErrPage /> */}

        </div>
            
        </>
    )
}

