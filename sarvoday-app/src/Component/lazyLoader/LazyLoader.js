const LazyLoader = () => {
  return (
    <div className="loader_wrap">
      <div className="loader_round" />
      <div className="loader_spin">
        {/* <img
          src={require("assets/images/loader/gif.gif").default}
          className="loader_img img-responsive"
          alt="loading"
        /> */}
        loading
      </div>
    </div>
  );
};

export default LazyLoader;
