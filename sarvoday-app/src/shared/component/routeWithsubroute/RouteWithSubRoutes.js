import React from "react";
import { Route, Redirect } from "react-router-dom";
import localStorageService from "../../../services";
import { routeConstants } from "../../constants/index";

const RouteWithSubRoutes = (route) => {
    const userData = localStorageService.getUser();
  return (
    <Route
      path={route.path}
      exact={!!route.exact}
      render={(props) => {
        if (route.protected) {
          if (userData) {
            return (
              // pass the sub-routes down to keep nesting
              <route.component {...props} routes={route.routes} />
            );
          } else {
            return (
              <Redirect
                to={{
                  pathname: routeConstants.AUTH.subRoutes.LOGIN.path,
                  state: { from: props.location },
                }}
              />
            );
          }
        } else if (route.protected === false) {
          if (!userData) {
            return (
              // pass the sub-routes down to keep nesting
              <route.component {...props} routes={route.routes} />
            );
          } else {
            return <Redirect to={routeConstants.HOME.path} />;
          }
        } else {
          return (
            // pass the sub-routes down to keep nesting
            <route.component {...props} routes={route.routes} />
          );
        }
      }}
    />
  );
};

export default RouteWithSubRoutes;