export default Object.freeze({
  HOME: {
    name: "Home",
    path: "/home",
  },
  GALLARY: {
    name: "gallery",
    path: "/gallery",
  },
  ROOM_CATALOGUE: {
    name: "room_catalouge",
    path: "/room-catalouge",
  },
  RESERVSTION_FORM: {
    name: "reservation_form",
    path: "/reservation_form",
  },
  RESTURENT: {
    name: "Resturent",
    path: "/resturent"
  },
  AUTH: {
    name: "Auth",
    path: "/auth",
    protected: false,
    subRoutes: {
      REGISTRATION: {
        name: "Registration",
        path: "/auth/registration",
      },
      LOGIN: {
        name: "Login",
        path: "/auth/login",
      },
    },
  },
});